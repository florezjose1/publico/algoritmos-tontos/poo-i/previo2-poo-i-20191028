#### Universidad

A continuación Ejercicio de entrenamiento Previo I de POO - 2019


Se anexa:

[Enunciado](enunciado.pdf)


* Diagrama UML

[Diagrama detallado PDF](diagramasUML/uml-nomina-full-detalles.pdf)

![Diagrama](diagramasUML/uml-examen-nomina-simple.png)






Versión Bluej: `4.2.2`

Versión Java: `11.0.6`

Hecho con ♥ por [Jose Florez](https://joseflorez.co)
