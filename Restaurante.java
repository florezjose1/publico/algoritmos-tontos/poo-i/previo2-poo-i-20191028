
//Complete
import java.util.ArrayList;

public class Restaurante {

    //Complete

    protected int egresosPorCompras;

    protected int ingresosPorVentas;

    protected Almuerzo almuerzo;

    protected ArrayList<Ingrediente> ingredientes;

    public Restaurante() {
        //Complete
        this.ingredientes = new ArrayList<Ingrediente>();
    }

    /**Busca un Ingrediente por el nombre y regresa el objeto dentro del ArrayList*/
    public Ingrediente buscarIngrediente(String nombre) {
        //Complete
        for(int i = 0; i < ingredientes.size(); i++)
            if(this.ingredientes.get(i).getNombre().equals(nombre)) return this.ingredientes.get(i);

        return null;
    }

    /*Regresa un array simple con los nombres String de todas las frutas*/
    public String [ ] getNombresFrutas() {
        //Complete
        ArrayList<String> f = new ArrayList();
        for(Ingrediente i : this.ingredientes)
            if(i instanceof Fruta) f.add(i.getNombre());

        String[] a = new String[f.size()];
        for(int j = 0; j < f.size(); j++) 
            a[j] = f.get(j);
        return a;
    }

    /*Regresa un array simple con los nombres String de todas las proteinas*/
    public String [ ] getNombresProteinas() {
        //Complete
        ArrayList<String> p = new ArrayList();
        for(Ingrediente i : this.ingredientes)
            if(i instanceof Proteina) p.add(i.getNombre());

        String[] a = new String[p.size()];
        for(int j = 0; j < p.size(); j++) 
            a[j] = p.get(j);
        return a;
    }

    /*Regresa un array simple con los nombres String de todos los granos*/
    public String [ ] getNombresGranos() {
        //Complete
        ArrayList<String> g = new ArrayList();
        for(Ingrediente i : this.ingredientes)
            if(i instanceof Grano) g.add(i.getNombre());

        String[] a = new String[g.size()];
        for(int j = 0; j < g.size(); j++) 
            a[j] = g.get(j);
        return a;
    }

    /*Regresa un array simple con los nombres String de todos las almidones*/
    public String [ ] getNombresAlmidones() {
        //Complete
        ArrayList<String> a = new ArrayList();
        for(Ingrediente i : this.ingredientes)
            if(i instanceof Almidon) a.add(i.getNombre());

        String[] n = new String[a.size()];
        for(int j = 0; j < a.size(); j++) 
            n[j] = a.get(j);
        return n;
    }

    /*Regresa un array simple con los nombres String de todos los ingredientes, no importa el tipo*/
    public String [ ] getNombresIngredientes() {
        //Complete
        String[] a = new String[this.ingredientes.size()];
        for(int j = 0; j < this.ingredientes.size(); j++) 
            a[j] = this.ingredientes.get(j).getNombre();
        return a;
    }

    /*Registra la compra de un Ingrediente*/
    public void registrarCompraIngrediente(String nombre, int cantidad, int precioCompra, int tipo) {
        //Complete
    }

    /*Calcula el precio de venta de un Almuerzo, dados los ingredientes deseados*/
    public int consultarPrecioVenta(String fruta, String proteina, String grano, String almidon) {
        //Complete
        Almuerzo almuerzo = new Almuerzo(this, fruta, proteina, grano, almidon);
        return almuerzo.getPrecioVenta();
    }

    /*Verifica si se puede preparar un almuerzo, si hay ingredientes suficientes*/
    public boolean sePuedePreparar(String fruta, String proteina, String grano, String almidon) {
        //Complete
        Almuerzo almuerzo = new Almuerzo(this, fruta, proteina, grano, almidon);
        return almuerzo.sePuedePreparar();
    }

    /*Registra la venta de un almuerzo. Primero valida si se puede preparar el plato
     * Debe lanzar un error RuntimeException cuando no hay suficientes ingredientes
     */
    public void registrarVenta(String fruta, String proteina, String grano, String almidon) {
        //Complete
        Almuerzo almuerzo = new Almuerzo(this, fruta, proteina, grano, almidon);
        if(almuerzo.sePuedePreparar()){
            almuerzo.registrarVenta();
            ingresosPorVentas += almuerzo.getPrecioVenta();
        }
        else throw new RuntimeException("No hay suficientes ingredientes...");
    }

    public int getIVA() {
        return this.ingresosPorVentas * 19 / 100;//Esto es un regalo ;)
    }

    public int getGananciasNetas() {
        return this.ingresosPorVentas - this.egresosPorCompras - this.getIVA(); //Esto es un regalo ;)
    }

    //Complete GET/SET
    
    /**Getter method ingredientes*/
    public java.util.ArrayList<Ingrediente> getIngredientes(){
        return this.ingredientes;
    }//end method getIngredientes

    /**Setter method ingredientes*/
    public void setIngredientes(java.util.ArrayList<Ingrediente> ingredientes){
        this.ingredientes = ingredientes;
    }//end method setIngredientes

    /**Getter method egresosPorCompras*/
    public int getEgresosPorCompras(){
        return this.egresosPorCompras;
    }//end method getEgresosPorCompras

    /**Setter method egresosPorCompras*/
    public void setEgresosPorCompras(int egresosPorCompras){
        this.egresosPorCompras = egresosPorCompras;
    }//end method setEgresosPorCompras

    /**Getter method ingresosPorVentas*/
    public int getIngresosPorVentas(){
        return this.ingresosPorVentas;
    }//end method getIngresosPorVentas

    /**Setter method ingresosPorVentas*/
    public void setIngresosPorVentas(int ingresosPorVentas){
        this.ingresosPorVentas = ingresosPorVentas;
    }//end method setIngresosPorVentas
}
