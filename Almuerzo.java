
public class Almuerzo {

    private static final int PORCION_FRUTA = 30;
    private static final int PORCION_PROTEINA = 200;
    private static final int PORCION_GRANO = 100;
    private static final int PORCION_ALMIDON = 50;
    
    protected Restaurante restaurante;
    protected Fruta fruta;
    protected Proteina proteina;
    protected Grano grano;
    protected Almidon almidon;

    //Complete

    public Almuerzo(Restaurante restaurante, String nombreFruta, String nombreProteina, String nombreGrano, String nombreAlmidon) {
        this.restaurante = restaurante;
        try {
            this.fruta = (Fruta) this.restaurante.buscarIngrediente(nombreFruta);
            this.proteina = (Proteina) this.restaurante.buscarIngrediente(nombreProteina);
            this.grano = (Grano) this.restaurante.buscarIngrediente(nombreGrano);
            this.almidon = (Almidon) this.restaurante.buscarIngrediente(nombreAlmidon);
            
            if(this.fruta == null || this.proteina == null || this.grano == null || this.almidon == null) 
                throw new RuntimeException("No hay suficientes ingredientes...");
            
        } catch (Exception e) {
            throw new RuntimeException("Eror en clase Almuerzo: " + e.getMessage());
        }
    }

    /*Calcula el precio de venta de este Almuerzo*/
    public int getPrecioVenta() {
       //Complete
       int pf = this.fruta.getPrecioCompra() * this.PORCION_FRUTA;
       int pp = this.proteina.getPrecioCompra() * this.PORCION_PROTEINA;
       int pg = this.grano.getPrecioCompra() * this.PORCION_GRANO;
       int pa = this.almidon.getPrecioCompra() * this.PORCION_ALMIDON;
       int s = pf+pp+pg+pa;
       return s*35/100 + s;
    }

    /*Verifica si se puede preparar este Almuerzo*/
    public boolean sePuedePreparar() {
        //Complete
        return this.fruta.getCantidadDisponible() >= this.PORCION_FRUTA &&
               this.proteina.getCantidadDisponible() >= this.PORCION_PROTEINA &&
               this.grano.getCantidadDisponible() >= this.PORCION_GRANO &&
               this.almidon.getCantidadDisponible() >= this.PORCION_ALMIDON;
    }
    
    /*Le informa a cada ingrediente de este Almuerzo que se realizo una venta, para que se registre*/
    public void registrarVenta(){
        //Complete
        this.fruta.registrarVenta(PORCION_FRUTA);
        this.proteina.registrarVenta(PORCION_PROTEINA);
        this.grano.registrarVenta(PORCION_GRANO);
        this.almidon.registrarVenta(PORCION_ALMIDON);
    }
    
    public boolean equals(Object obj){
      //Complete
      if(obj instanceof Almuerzo) 
        return ((Almuerzo)obj).restaurante.equals(this.restaurante)
        &&((Almuerzo)obj).fruta.equals(this.fruta)
        &&((Almuerzo)obj).grano.equals(this.grano)
        &&((Almuerzo)obj).proteina.equals(this.proteina)
        &&((Almuerzo)obj).almidon.equals(this.almidon);
        return false;
    }
}
