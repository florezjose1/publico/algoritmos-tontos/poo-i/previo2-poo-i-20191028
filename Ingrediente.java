/*Complete*/ 
public abstract class Ingrediente {

    private String nombre;

    private int precioCompra;

    private int cantidadDisponible;

    /*Constructor Vacio*/
    public /*Complete*/ Ingrediente(){

    }

    /*Constructor con parametro nombre*/
    public /*Complete*/Ingrediente(String nombre){
        //Complete
        this.nombre = nombre;
    }

    /*Constructor con todos los parametros*/
    public /*Complete*/Ingrediente(String nombre, int cantidadDisponible, int precioCompra){
        //Complete
        this(nombre);
        this.cantidadDisponible = cantidadDisponible;
        if(precioCompra > this.precioCompra) this.precioCompra = precioCompra;
    }

    /*Registra una compra de este tipo de Ingrediente*/
    public void registrarCompra(int cantidad, int precioCompra) {
        //Complete
        this.cantidadDisponible += cantidad;
        if(precioCompra > this.precioCompra) this.precioCompra = precioCompra;
    }

    /*Registra una venta de este tipo de Ingrediente*/
    public void registrarVenta(int cantidad){
        //Complete
        this.cantidadDisponible -= cantidad;
    }

    //Complete GET/SET

    public boolean equals(Object obj){
        //Complete
        if(obj instanceof Ingrediente){
            Ingrediente i = (Ingrediente)(obj);
            return i.getNombre() == this.getNombre() && 
                   i.getPrecioCompra() == this.getPrecioCompra() && 
                   i.getCantidadDisponible() == this.getCantidadDisponible();
        }
        if(obj instanceof String) 
            return obj.equals(this.nombre) && obj.equals(this.precioCompra);
            
        return false;
    }

    public String toString(){
        //Complete
        String str = "";
        str = this.nombre + "," + this.precioCompra + "," + this.cantidadDisponible;
        return str;
    }

    //Start GetterSetterExtension Source Code
    /**GET Method Propertie nombre*/
    public String getNombre(){
        return this.nombre;
    }//end method getNombre

    /**SET Method Propertie nombre*/
    public void setNombre(String nombre){
        this.nombre = nombre;
    }//end method setNombre

    /**GET Method Propertie precioCompra*/
    public int getPrecioCompra(){
        return this.precioCompra;
    }//end method getPrecioCompra

    /**SET Method Propertie precioCompra*/
    public void setPrecioCompra(int precioCompra){
        this.precioCompra = precioCompra;
    }//end method setPrecioCompra

    /**GET Method Propertie cantidadDisponible*/
    public int getCantidadDisponible(){
        return this.cantidadDisponible;
    }//end method getCantidadDisponible

    /**SET Method Propertie cantidadDisponible*/
    public void setCantidadDisponible(int cantidadDisponible){
        this.cantidadDisponible = cantidadDisponible;
    }//end method setCantidadDisponible

    //End GetterSetterExtension Source Code
    //!
}
